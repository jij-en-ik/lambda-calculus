#Dit is de class voor een abstracte lambdaterm.
class LambdaTerm:
    """Abstract Base Class for lambda terms."""

#Dit is de class voor een variable
class Variable(LambdaTerm):
    """Represents a variable."""

    def __init__(self, symbol): #Een variable heeft één input, symbol.
        self.symbol = symbol

    def __repr__(self): #Wanner je een variable representeert, geeft hij terug dat het een variable is
        return f"Variable('{self.symbol}')"

    def __str__(self): #De string van een variable is de string van het symbool
        return str(self.symbol)
    
    def __eq__(self, other): #Met deze equivallentie kan je checken of twee variabelen hetzelfde zijn. Door te kijken of de symbolen hetzelfde zijn.
        if type(other) == Variable:
            return self.symbol == other.symbol
        else:
            return False
        
    def __hash__(self):
        return hash(repr(self))

#Dit is de class voor een abstraction.
class Abstraction(LambdaTerm):
    """Represents a lambda term of the form (λx.M)."""

    def __init__(self, variable, body): #Een abstraction neemt twee inputs, de variable en body.
        self.var = variable
        self.body = body

    def __repr__(self): #Bij de representatie laat hij zien dat het een abstraction is.
        return f"Abstraction({repr(self.var)}, {repr(self.body)})"

    def __str__(self): #De string van een abstraction is een lambda met daarachter de variable van de abstraction. En daarachter weer een punt met de body.
        return f"λ{self.var}. {self.body}"

    def __call__(self, argument): 
        return Application(self, argument)

#Dit is de class voor een Application.
class Application(LambdaTerm):
    """Represents a lambda term of the form (M N)."""

    def __init__(self, function, argument): #Een application heeft twee inputs een function en argument, die weer zelf een variable, abstraction of application kunnen zijn.
        self.function = function
        self.arg = argument

    def __repr__(self): #Wanneer je een Application represent wordt verwezen naar de representatie van de function en argument.
        return f"Application({repr(self.function)}, {repr(self.arg)})"

    #De method voor het weergeven van een Application als string.
    #Deze manier zorgt voor overzichtelijke haakjes die assisteren bij het lezen van de lambda term.
    def __str__(self):
        if type(self.function) == Abstraction and type(self.arg) == Variable:
           return f"({str(self.function)}) {str(self.arg)}"
        elif type(self.function) == Application and type(self.arg) == Variable:
            return f"({str(self.function)}) {str(self.arg)}"
        elif type(self.function) == Variable and type(self.arg) == Variable:
            return f"{str(self.function)} {str(self.arg)}"
        elif type(self.function) == Variable:
            return f"{str(self.function)} ({str(self.arg)})"
        elif type(self.function) == Abstraction and type(self.arg) == Abstraction:
            return f"({str(self.function)}) {str(self.arg)}"
        elif type(self.function) == Application and type(self.arg) == Abstraction:
            return f"({str(self.function)}) {str(self.arg)}"
        else:
            return f"({str(self.function)}) ({str(self.arg)})"