import ply.lex as lex
#Dit zijn alle symbolen die we tegen kunnen komen in een lambda term.
tokens = (
    'VARIABLE',
    'LPAREN',
    'RPAREN',
    'PERIOD',
    'LAMBDA',
    )

#Hieronder wordt voor de tokens duidelijk gedefinieerd wat het precies is.
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_PERIOD = r'\.'
def t_LAMBDA(t):
    r'\\|λ'
    return t

#Een spatie wordt niet bekeken door de lexer, want een spatie heeft geen invloed op de lambdaterm.
t_ignore = ' '

#Een variable kan een kleine letter of hoofdletter zijn.
def t_VARIABLE(t):
    r'[a-zA-Z]'
    return t

#Wanneer de lexer een symbool tegenkomt wat hij niet herkent dan negeert hij hem en gaat hij verder bij het volgende symbool.
def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)

#Hier wordt de lexer gemaakt.
lexer = lex.lex()