import copy
from LambdaClasses import LambdaTerm, Variable, Abstraction, Application
import lambdaparser

#Dit is de functie die substitutie toepast op lambda termen.
#In het programma wordt eerst gekeken of de term een Variable, Abstraction of Application is.
#term is de term waarop substitutie wordt uitgevoerd, svar is de variable die wordt vervangen en sterm is waar het mee wordt vervangen.
def substitution(term, svariable, sterm):
    '''Voert substitutie uit op een lambda term'''
    if type(term) == Variable:
        if term == svariable:
            return sterm
        else:
            return term
    elif type(term) == Abstraction:
        if term.var == svariable:
            return term
        else:
            return Abstraction(term.var, substitution(term.body, svariable, sterm))
    elif type(term) == Application:
        return Application(substitution(term.function, svariable, sterm), substitution(term.arg, svariable, sterm))


#Deze functie wordt gebruikt bij de functie voor beta-reductie.
def r_substitution(term, svariable, sterm): #vervangt elke instance van svariable met sterm in de term.
    '''Voert een substitutie uit voor beta-reductie'''
    if type(term) == Variable:
        if term.symbol == svariable.symbol: #als de Variable hetzelfde is als degene die je aan moet passen pas je hem aan, anders returnt de functie de term.
            return copy.deepcopy(sterm)

    elif type(term) == Abstraction:         
        if term.var == svariable:
            term.var = r_substitution(term.var, svariable, sterm)
        term.body = r_substitution(term.body, svariable, sterm)

    elif type(term) == Application:         #als de term een Application is moet in de function én de argument de svariable gesubstitueerd worden voor de sterm.
        term.function = r_substitution(term.function, svariable, sterm)
        term.arg = r_substitution(term.arg, svariable, sterm)
    
    return copy.deepcopy(term)



#Dit is de functie die beta-reductie toepast op een gegeven lambda term.
def reduction(term):
    '''Voert beta-reductie uit op een lambda term'''
    if type(term) == Variable: #een variable kan niet gereduced worden.
        pass

    elif type(term) == Abstraction: #de Abstraction.var is een variable, dus hoeft niet gereduced te worden, alleen de body.
        term.body = reduction(term.body)

    elif type(term) == Application:
        if type(term.function) == Abstraction:  #als de Application.function een Abstraction is moet je in die Abstraction de function.var substitueren voor term.arg.
            return r_substitution(term.function.body, term.function.var, term.arg)
        else:                                   #anders moet je de Application.function reducen tot die niet meer verandert (en het dus een Variable is).
            newfunction = reduction(term.function)
            if newfunction != term.function:
                term.function = newfunction
            else:
                term.arg = reduction(term.arg)
                
    return term



#Dit is de functie die de parser vertaalt naar een functie.
def fromstring(term):
    '''Maakt van een string een lambda term'''
    return lambdaparser.parser.parse(term)



#Dit is de functie die de reduction functie uit blijft voeren tot er geen reductie meer mogelijk is.
#Als output geeft de functie een string met alle reductie stappen.
def vreduce(term):
    '''Voert alle reductie stappen uit'''
    if type(term) == Variable or type(term) == Abstraction: #Eerst wordt gekeken of de term een Variable of Abstracrtion is, want in die gevallen is er geen reduction mogelijk.
        return term

    elif type(term) == Application: 
        t = term
        reductions = [] #Dit is de lijst waarin alle reductie stappen worden opgeslagen.
        string_lijst = str(t)

        while(True):
            term_string = str(t)
            reduction_term = reduction(t)
            reduction_term_string = str(reduction_term)
            if term_string == reduction_term_string: #Hier wordt gekeken of reductie nog iets verandert, zoniet dan wordt de string gemaakt en gereturend.
                reductions.append(reduction_term)
                for i in range(1,len(reductions),2):
                    string_lijst = string_lijst + ' --> ' + str(reductions[i])
                return string_lijst

            else: #Wanneer reductie nog wel iets verandert wordt er verder gereduceerd.
                reductions.append(term_string)
                reductions.append(reduction_term_string)
                t = reduction_term