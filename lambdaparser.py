import ply.yacc as yacc
from LambdaClasses import LambdaTerm, Variable, Abstraction, Application

#Hieronder staat de BNF grammar voor lambda calculus die de parser gebruikt en in de code wordt gebruikt.
#BNF Grammar voor lambda calculus.
#TERM ::= LPAREN TERM RPAREN
#       | VARIABLE
#       | LAMBDA VARIABLE PERIOD TERM
#       | TERM TERM 
#


#Tokens importen van de lexer.
from lambdalexer import tokens

#Hier wordt aangegeven dat VARIABLE links associatief.
precedence = (
    ('left', 'VARIABLE'),
)

#Hier wordt regel 1 toegepast.
def p_paren(p):
    ''' TERM : LPAREN TERM RPAREN %prec VARIABLE'''
    p[0] = p[2]

#Hier wordt regel 2 toegepast.
def p_variable(p):
    ''' TERM : VARIABLE '''
    p[0] = Variable(p[1])

#Hier wordt regel 3 toegepast.
def p_abstraction(p):
    '''TERM : LAMBDA VARIABLE PERIOD TERM
            | LAMBDA VARIABLE VARIABLE PERIOD TERM
            | LAMBDA VARIABLE VARIABLE VARIABLE PERIOD TERM
            | LAMBDA VARIABLE VARIABLE VARIABLE VARIABLE PERIOD TERM'''
    output = Abstraction(Variable(p[len(p)-3]), p[len(p)-1])
    for i in reversed(range(2, len(p)-3)):
        output = Abstraction(Variable(p[i]), output)
    p[0] = output

#Hier wordt regel 4 toegepast.
def p_application(p):
    ''' TERM : TERM TERM %prec VARIABLE'''
    p[0] = Application(p[1], p[2])

#Hier wordt er een error gegeven als er een symbool op een ongewone plek staat.
def p_error(p):
    print("Syntax error in input!")
 
#Hier wordt de parser gemaakt.
parser = yacc.yacc()