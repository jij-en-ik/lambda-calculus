from functies import fromstring, vreduce, substitution
from LambdaClasses import Variable
#De interpreter file voor de input van de terminal.
while True:
    print("Wilt u een term reduceren of substitueren? (r/s)") #Er wordt eerst gevraagd of je wil reducen of substituten.
    keuze = input()
    if keuze in ["r", "R"]: #Hier wordt reductie uitgevoerd.
        print('Vul uw lambda term in')
        term = input()

        lterm = fromstring(eval(f"r\"{term}\""))
        nlterm = lterm
        print('Uw lambda term is ' + str(nlterm))
        print(' ')

        vrterm = vreduce(nlterm)
        print('De reductie met stappen luidt als volgt ' + str(vrterm))
        break

    elif keuze in ["s", "S"]: #Hier wordt substitutie uitgevoerd.
        print ("Vul uw lambda term in")
        term = input()
        print("Vul de variabele die weggesubstitueerd moet worden")
        wegvar = Variable(input())
        print("Vul de variabele in waarvoor gesubstitueerd moet worden")
        nieuwvar = Variable(input())

        lterm = fromstring(eval(f"r\"{term}\""))
        print("Uw lambda term is ", str(lterm))
        subterm = substitution(lterm, wegvar, nieuwvar)
        print("Na substitutie is de nieuwe lambda term", str(subterm))
        break

    else: #Wanneer je geen r of s invoerd begin je weer opnieuw met de loop.
        print("Dat is geen mogelijke input")
        continue