# Lambda Calculator

## Omschrijving
Lambda Calculator is een bestand wat in beta-reductie en substitutie uit kan voeren op lambda termen.

## Installatie
Download het project als een .zip en pak het uit in één map, vervolgens werken alle files samen. Als dat is gelukt kan je de interpreter runnen om zo het bestand te gebruiken. De input moet gegeven worden in normale lambda-notatie, met een \ voor lambda. De interpreter geeft ook terug hoe de parser het leest. Als daar iets misgaat, helpt extra haakjes zetten vaak om de volgorde duidelijker te krijgen.

## Gebruik
In de interpreter moet de input worden gegeven in normale lambda-notatie, met een \ voor lambda. De interpreter geeft ook terug hoe de parser het leest. Als daar iets misgaat, helpt extra haakjes zetten vaak om de volgorde duidelijker te krijgen. Je hoeft er niet op te letten of je wel of geen spaties neerzet.

Hieronder een klein voorbeeld van hoe je de interpreter kan gebruiken, wanner je hem runt en een term wil reducen.
Alles wat als string is weergegeven is wat de interpreter geeft als ouput, al het andere is wat je zelf in moet vullen.

```python

'Wilt u een term reduceren of substitueren? (r/s)'
r
'Vul uw lambda term in'
(\x y z. z (y x))a(\w.w)c
'Uw lambda term is (((λx. λy. λz. z (y x)) a) λw. w) c'

'De reductie met stappen luidt als volgt (((λx. λy. λz. z (y x)) a) λw. w) c --> ((λy. λz. z (y a)) λw. w) c --> (λz. z ((λw. w) a)) c --> c ((λw. w) a) --> c a'
```

Hieronder nog een ander voorbeeld maar dan voor substitutie.

```python

'Wilt u een term reduceren of substitueren? (r/s)'
s
'Vul uw lambda term in'
\x y. z x x y
'Vul de variabele die weggesubstitueerd moet worden'
z
'Vul de variabele in waarvoor gesubstitueerd moet worden'
t
'Uw lambda term is  λx. λy. ((z x) x) y'
'Na substitutie is de nieuwe lambda term λx. λy. ((t x) x) y'
```

## Roadmap
Als er aan de toekomst nog gewerkt gaat worden aan dit bestand, dan zouden we nog een paar dingen willen implementeren.

-Church Numerals

-Combinators

-De Bruijn Index/Alpha conversion

## Bijdragers

- Stijn Pellenkoft

- Pjotr Vugts

